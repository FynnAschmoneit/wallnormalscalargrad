/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013-2017 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "wallNormalScalarGrad.H"
#include "volFields.H"
#include "surfaceFields.H"
#include "turbulentTransportModel.H"
#include "turbulentFluidThermoModel.H"
#include "wallPolyPatch.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace functionObjects
{
    defineTypeNameAndDebug(wallNormalScalarGrad, 0);
    addToRunTimeSelectionTable(functionObject, wallNormalScalarGrad, dictionary);
}
}


// * * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * //

void Foam::functionObjects::wallNormalScalarGrad::writeFileHeader(const label i)
{
    // Add headers to output data
    writeHeader(file(), "wall normal gradient");
    writeCommented(file(), "Time");

    const fvPatchList& patches = mesh_.boundary();
    forAllConstIter(labelHashSet, patchSet_, iter)
    {
	label patchi = iter.key();
        const fvPatch& pp = patches[patchi];
	writeTabbed(file(), "mean "+pp.name() );       
	writeTabbed(file(), "stdDev "+pp.name() );
    }

    file() << endl;
}


void Foam::functionObjects::wallNormalScalarGrad::calcScalarGrad
(
    const volVectorField& grdField,
    volScalarField& normalGrad
)
{
    forAllConstIter(labelHashSet, patchSet_, iter)
    {
        label patchi = iter.key();

        scalarField& ssp = normalGrad.boundaryFieldRef()[patchi];
        const vectorField& Sfp = mesh_.Sf().boundaryField()[patchi];
        const scalarField& magSfp = mesh_.magSf().boundaryField()[patchi];
	const vectorField& grdFieldP = grdField.boundaryField()[patchi];

	ssp = (-Sfp/magSfp) & grdFieldP;
    }
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::functionObjects::wallNormalScalarGrad::wallNormalScalarGrad
(
    const word& name,
    const Time& runTime,
    const dictionary& dict
)
:
    fvMeshFunctionObject(name, runTime, dict),
    logFiles(obr_, name),
    writeLocalObjects(obr_, log),
    patchSet_()
{
    volScalarField* wallNormalScalarGradPtr
    (
        new volScalarField
        (
            IOobject
            (
                type(),
                mesh_.time().timeName(),
                mesh_,
                IOobject::NO_READ,
                IOobject::NO_WRITE
            ),
            mesh_,
            dimensionedScalar
            (
                "0",
                sqr(dimLength)/sqr(dimTime),
                Zero
            )
        )
    );

    mesh_.objectRegistry::store(wallNormalScalarGradPtr);

    read(dict);
    resetName(typeName);
    resetLocalObjectName(typeName);
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::functionObjects::wallNormalScalarGrad::~wallNormalScalarGrad()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

bool Foam::functionObjects::wallNormalScalarGrad::read(const dictionary& dict)
{
    fvMeshFunctionObject::read(dict);
    writeLocalObjects::read(dict);

    fieldName = dict.lookupOrDefault<word>("field", "s");
    
    const polyBoundaryMesh& pbm = mesh_.boundaryMesh();

    patchSet_ =
        mesh_.boundaryMesh().patchSet
        (
            wordReList(dict.lookupOrDefault("patches", wordReList()))
        );

    Info<< type() << " " << name() << ":" << nl;

    if (patchSet_.empty())
    {
        forAll(pbm, patchi)
        {
            if (isA<wallPolyPatch>(pbm[patchi]))
            {
                patchSet_.insert(patchi);
            }
        }

        Info<< "    processing all wall patches" << nl << endl;
    }
    else
    {
        Info<< "    processing wall patches: " << nl;
        labelHashSet filteredPatchSet;
        forAllConstIter(labelHashSet, patchSet_, iter)
        {
            label patchi = iter.key();
            if (isA<wallPolyPatch>(pbm[patchi]))
            {
                filteredPatchSet.insert(patchi);
                Info<< "        " << pbm[patchi].name() << endl;
            }
            else
            {
                WarningInFunction
                    << "Requested wall shear stress on non-wall boundary "
                    << "type patch: " << pbm[patchi].name() << endl;
            }
        }

        Info<< endl;

        patchSet_ = filteredPatchSet;
    }

    return true;
}


bool Foam::functionObjects::wallNormalScalarGrad::execute()
{
    volScalarField& wallNormalScalarGrad =
        mesh_.lookupObjectRef<volScalarField>(type());

    volScalarField& internalScalarField =
      mesh_.lookupObjectRef<volScalarField>(fieldName);

    volVectorField vfGradS = -fvc::grad( internalScalarField );
   
    calcScalarGrad(vfGradS, wallNormalScalarGrad);

    return true;
}


bool Foam::functionObjects::wallNormalScalarGrad::write()
{

  Log << type() << " " << name() << " write:" << nl << endl;

    writeLocalObjects::write();

    logFiles::write();

    const volScalarField& wallNormalScalarGrad =
        obr_.lookupObject<volScalarField>(type());

    const fvPatchList& patches = mesh_.boundary();

     if (Pstream::master())
    {
	file() << mesh_.time().value() ;
    }
    
    forAllConstIter(labelHashSet, patchSet_, iter)
    {
        label patchi = iter.key();
        const fvPatch& pp = patches[patchi];
        const scalarField& ssp = wallNormalScalarGrad.boundaryField()[patchi];
	const scalarField& magSfp = mesh_.magSf().boundaryField()[patchi];               // face area field
	scalar sumArea	 	= gSum( magSfp );
	scalar meanGrad		= gSum( ssp*magSfp/sumArea );
	scalar meanSqrdGrad	= gSum( ssp*ssp*magSfp ) /sumArea ;
	
        scalar minSsp = gMin( ssp );
        scalar maxSsp = gMax( ssp );
	scalar alpha = 1.0/( gSum(ssp/ssp) -1.0);
	scalar stdDev = sqrt( alpha*( meanSqrdGrad - meanGrad*meanGrad ) );
        Log << "    min/max/mean/stdDev(" << pp.name() << ") = "
            << minSsp << ", " << maxSsp << ", " << meanGrad << ", " << stdDev << endl;

	if (Pstream::master())
	{
	    file() << tab << meanGrad << tab << stdDev ;
	}
    }

    if (Pstream::master())
    {
	file() << endl;
    }
    
    Log << endl;

    return true;
}


// ************************************************************************* //
